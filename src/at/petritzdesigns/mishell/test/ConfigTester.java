package at.petritzdesigns.mishell.test;

import at.petritzdesigns.mishell.helper.Configuration;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class ConfigTester {

    public static void main(String[] args) {
        try {
            if (!Configuration.getDefault().set("test", "Test")) {
                System.out.println("Test failed... Could not set Configuration item");
                System.exit(0);
            }

            if (!"Test".equals(Configuration.getDefault().get("test"))) {
                System.out.println("Test failed... Could not get Configuration item");
                System.exit(0);
            }

            if (!Configuration.getDefault().delete("test")) {
                System.out.println("Test failed... Could not delete Configuration item");
                System.exit(0);
            }

            if (!Configuration.getDefault().getBool("test2")) {
                System.out.println("Test failed... Could not get Boolean");
                System.exit(0);
            }
            
            System.out.println("Test passed!");
        } catch (Exception ex) {
            System.err.println(ex);
            System.err.println("Test failed");
        }
    }

}
