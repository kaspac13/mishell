package at.petritzdesigns.mishell.test;

import at.petritzdesigns.mishell.helper.LogType;
import at.petritzdesigns.mishell.helper.Logger;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class LogTester {

    public static void main(String[] args) {
        try {
            Logger.getDefault().log(new Exception("Test#1"), false);
            Logger.getDefault().log("Test#2", false);
            Logger.getDefault().log(LogType.LOG_INFO, new Exception("Test#3"), false);
            Logger.getDefault().log(LogType.LOG_ERROR, "Test#4", false);
        } catch (Exception ex) {
            System.out.println("Test failed...");
            System.err.println(ex);
        }

        System.out.println("Test passed");
    }

}
