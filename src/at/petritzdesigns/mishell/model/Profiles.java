package at.petritzdesigns.mishell.model;

import at.petritzdesigns.mishell.data.Profile;
import at.petritzdesigns.mishell.data.ProfileColumnNames;
import at.petritzdesigns.mishell.helper.Configuration;
import at.petritzdesigns.mishell.view.chooser.FileChooser;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * MiShell
 *
 * @author Paul Kasper
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class Profiles extends AbstractTableModel {

    private List<Profile> profiles = new ArrayList<>();

    public void add(Profile profile) throws IOException {
        profiles.add(profile);
        super.fireTableDataChanged();
        writeTo();
    }

    public void delete(int index) throws Exception {
        if (index < 0 || index > profiles.size()) {
            throw new Exception("The remove index is invalid!");
        }
        profiles.remove(index);
        super.fireTableDataChanged();
    }

    public void remove(Profile value) throws Exception {
        if (!profiles.contains(value)) {
            throw new Exception("Weathervalue not found!!!");
        }

        profiles.remove(value);
        super.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return profiles.size();
    }

    @Override
    public int getColumnCount() {
        return ProfileColumnNames.values().length;
    }

    @Override
    public String getColumnName(int col) {
        return ProfileColumnNames.values()[col].getName();
    }

    @Override
    public Object getValueAt(int row, int col) {
        Profile p = profiles.get(row);
        ProfileColumnNames enumIndex = ProfileColumnNames.values()[col];

        switch (enumIndex) {
            case NAME:
                return p.getName();
            case FOREGROUND_COLOR:
                return p.getForegroundColorAsString();
            case BACKGROUND_COLOR:
                return p.getBackgroundColorAsString();
            case FONT_NAME:
                return p.getFontAsString();
            case FONT_STYLE:
                return p.getStyleAsString();
            case FONT_SIZE:
                return p.getSizeAsString();
            default:
                return "?";
        }
    }

    public void writeTo() {
        BufferedWriter bw = null;
        try {

            File file = new File(Configuration.getDefault().get("profiles_path"));
            createProfilesFileIfNotExits();
            bw = new BufferedWriter(new FileWriter(file));

            for (Profile profile : profiles) {
                profile.writeTo(bw);
            }
            bw.flush();
        } catch (Exception e) {
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (Exception e) {
                }
            }
        }

    }

    public void readFrom() throws IOException, Exception {
        BufferedReader br = null;

        FileChooser fc = new FileChooser();
        File file = new File(Configuration.getDefault().get("profiles_path"));
        createProfilesFileIfNotExits();

        br = new BufferedReader(new FileReader(file));

        profiles.clear();
        String line = "";
        while ((line = br.readLine()) != null) {
            Profile profile = new Profile(line);

            add(profile);
        }
    }

    private void createProfilesFileIfNotExits() throws IOException {
        File path = new File(Configuration.getDefault().get("profiles_path"));

        if (!path.getParentFile().exists()) {
            path.getParentFile().mkdirs();
        }
        if (!path.exists()) {
            path.createNewFile();
        }
    }
}
