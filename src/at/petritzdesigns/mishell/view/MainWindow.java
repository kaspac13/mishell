package at.petritzdesigns.mishell.view;

import at.petritzdesigns.mishell.data.Profile;
import at.petritzdesigns.mishell.helper.Logger;
import at.petritzdesigns.mishell.model.Profiles;
import at.petritzdesigns.mishell.shell.Environment;
import at.petritzdesigns.mishell.shell.Shell;
import at.petritzdesigns.mishell.view.chooser.ColorChooser;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JOptionPane;
import say.swing.JFontChooser;

/**
 * MiShell
 *
 * @author Paul Kasper
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */

/*
 To Do:

 Options
 Title: "MiShell - XYXY"
 - aktiver Befehl
 - Profilname
 - Eigene Gruppen
    
 ANSI Farben on/off

 */
public class MainWindow extends javax.swing.JFrame {

    private Profiles model = new Profiles();
    private final int style = Font.PLAIN;
    private Color foreground, background;
    private Font font;

    public MainWindow(Shell shell) {
        initComponents();
        Logger.getDefault().setTextPane(tpMain);
        shell.run(tpMain);
        tbProfiles.setModel(model);
        try {
            model.readFrom();
        }
        catch(Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        tbMain = new javax.swing.JTabbedPane();
        spMain = new javax.swing.JScrollPane();
        tpMain = new javax.swing.JTextPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cbProfiles = new javax.swing.JComboBox();
        pnProfiles = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbProfiles = new javax.swing.JTable();
        btAddProfile = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        lbName = new javax.swing.JLabel();
        tfName = new javax.swing.JTextField();
        btFontChooser = new javax.swing.JButton();
        btForegroundColor = new javax.swing.JButton();
        btBackgroundColor = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("MiShell");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                onClosing(evt);
            }
        });

        spMain.setViewportView(tpMain);

        tbMain.addTab("Shell", spMain);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Open with Profile:");
        jPanel1.add(jLabel1, new java.awt.GridBagConstraints());

        jPanel1.add(cbProfiles, new java.awt.GridBagConstraints());

        tbMain.addTab("Basic", jPanel1);

        pnProfiles.setLayout(new java.awt.GridBagLayout());

        jLabel2.setText("Font:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnProfiles.add(jLabel2, gridBagConstraints);

        jLabel3.setText("Foreground Color");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnProfiles.add(jLabel3, gridBagConstraints);

        tbProfiles.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbProfiles);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnProfiles.add(jScrollPane1, gridBagConstraints);

        btAddProfile.setText("Add to Profiles");
        btAddProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onAdd(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnProfiles.add(btAddProfile, gridBagConstraints);

        jLabel4.setText("Background Color:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnProfiles.add(jLabel4, gridBagConstraints);

        lbName.setText("Name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnProfiles.add(lbName, gridBagConstraints);

        tfName.setText("Profile1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnProfiles.add(tfName, gridBagConstraints);

        btFontChooser.setText("...");
        btFontChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onChooseFont(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnProfiles.add(btFontChooser, gridBagConstraints);

        btForegroundColor.setText("Foreground");
        btForegroundColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onChooseForegroundColor(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnProfiles.add(btForegroundColor, gridBagConstraints);

        btBackgroundColor.setText("Background");
        btBackgroundColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onChooseBackgroundColor(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnProfiles.add(btBackgroundColor, gridBagConstraints);

        tbMain.addTab("Profiles", pnProfiles);

        getContentPane().add(tbMain, java.awt.BorderLayout.CENTER);

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void onClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_onClosing
        //really close? -> JOptionPane
        //then close terminal session
        Environment.getInstance().applicationEnd();
    }//GEN-LAST:event_onClosing

    private void onChooseFont(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onChooseFont
        JFontChooser fontChooser = new JFontChooser();
        int result = fontChooser.showDialog(this);
        if(result == JFontChooser.OK_OPTION) {
            font = fontChooser.getSelectedFont();
            System.out.println("font: " + font);
        }
        else {
            //TODO error handling code
        }
        //TODO show font name and size as a button text
    }//GEN-LAST:event_onChooseFont

    private void onChooseForegroundColor(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onChooseForegroundColor
        ColorChooser cc = new ColorChooser();
        foreground = cc.getChoosenColor("foreground color");
    }//GEN-LAST:event_onChooseForegroundColor

    private void onAdd(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onAdd
        try {
            String name = tfName.getText();
            if (foreground == null) {
                JOptionPane.showMessageDialog(this, "Please choose a foreground color!");
            }
            if (background == null) {
                JOptionPane.showMessageDialog(this, "Please choose a background color!");
            }

            Profile profile = new Profile(name, foreground, background, font);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }//GEN-LAST:event_onAdd

    private void onChooseBackgroundColor(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onChooseBackgroundColor
        ColorChooser cc = new ColorChooser();
        background = cc.getChoosenColor("foreground color");
    }//GEN-LAST:event_onChooseBackgroundColor

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAddProfile;
    private javax.swing.JButton btBackgroundColor;
    private javax.swing.JButton btFontChooser;
    private javax.swing.JButton btForegroundColor;
    private javax.swing.JComboBox cbProfiles;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbName;
    private javax.swing.JPanel pnProfiles;
    private javax.swing.JScrollPane spMain;
    private javax.swing.JTabbedPane tbMain;
    private javax.swing.JTable tbProfiles;
    private javax.swing.JTextField tfName;
    private javax.swing.JTextPane tpMain;
    // End of variables declaration//GEN-END:variables

}
