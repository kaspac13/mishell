package at.petritzdesigns.mishell.view;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class NavigationFilterPrefixWithBackspace extends NavigationFilter {

    private final int length;
    private final Action deletePrevious;

    /*
     Taken from:
     http://stackoverflow.com/a/22941994/3575969
     */
    public NavigationFilterPrefixWithBackspace(int length, JTextComponent component) {
        this.length = length;
        deletePrevious = component.getActionMap().get("delete-previous");
        component.getActionMap().put("delete-previous", new BackspaceAction());
        component.setCaretPosition(length);
    }

    @Override
    public void setDot(NavigationFilter.FilterBypass fb, int dot, Position.Bias bias) {
        fb.setDot(Math.max(dot, length), bias);
    }

    @Override
    public void moveDot(NavigationFilter.FilterBypass fb, int dot, Position.Bias bias) {
        fb.moveDot(Math.max(dot, length), bias);
    }

    class BackspaceAction extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                JTextComponent component = (JTextComponent) e.getSource();

                if (component.getCaretPosition() > length) {
                    deletePrevious.actionPerformed(e);
                }
            } catch (Exception ex) {
                //no handling required
            }
        }
    }
}
