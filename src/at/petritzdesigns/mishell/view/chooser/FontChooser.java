package at.petritzdesigns.mishell.view.chooser;

import java.awt.Font;

/**
 * MiShell
 *
 * @author Paul Kasper
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */

public class FontChooser {

    Font font;

    public Font getFont() {
//        JFontChooser fontChooser = new JFontChooser();
//        int result = fontChooser.showDialog(parent);
//        if (result == JFontChooser.OK_OPTION) {
//            Font font = fontChooser.getSelectedFont();
//            System.out.println("Selected Font : " + font);
//        }
        return font;
    }

}
