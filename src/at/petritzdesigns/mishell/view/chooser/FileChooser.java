/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.petritzdesigns.mishell.view.chooser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JFileChooser;

/**
 *
 * @author User
 */
public class FileChooser {
    File file;

    public File getFile() {
        JFileChooser chooser = new JFileChooser("..\\mishell");
            int answer = chooser.showOpenDialog(null);
            if (answer == JFileChooser.APPROVE_OPTION) {
                file = chooser.getSelectedFile();
            }
        return file;
    }
    
    
}
