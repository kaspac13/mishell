package at.petritzdesigns.mishell.view.chooser;

import java.awt.Color;
import javax.swing.JColorChooser;

/**
 * MiShell
 *
 * @author Paul Kasper
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */

public class ColorChooser {
    Color choosenColor;

    public Color getChoosenColor(String colorType) {
        String message = "Please choose your "+colorType+"!";
        choosenColor = JColorChooser.showDialog(null, message , null);        
        return choosenColor;
    }
    
    
}