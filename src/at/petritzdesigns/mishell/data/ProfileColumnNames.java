package at.petritzdesigns.mishell.data;

/**
 * MiShell
 *
 * @author Paul Kasper
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */

public enum ProfileColumnNames {
    NAME("Name"), FOREGROUND_COLOR("Foreground Color")
    ,BACKGROUND_COLOR("Background Color")
    , FONT_NAME("Font"), FONT_STYLE("Style"), FONT_SIZE("Size");
    
    private String name;

    private ProfileColumnNames(String name) {
        this.name = name;
    }

    public String getName() {
        return name;  
    }
}
