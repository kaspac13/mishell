/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.petritzdesigns.mishell.data;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * MiShell
 *
 * @author Paul Kasper
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class Profile {
    /*
     To Do
     - JColorPicker https://docs.oracle.com/javase/tutorial/uiswing/components/colorchooser.html
     - JFontChooser ->  http://jfontchooser.osdn.jp/site/jfontchooser/project-summary.html
     - Font Klasse hat Schrifte, Größe
     - Font font = new Font("Jokerman", Font.PLAIN, 35);
     so ruft ma den JFontChooser dann auf wenn die .jar datei hinzugefügt worden is (bitte in an ordner namens lib reintun und in Netbeans unter Libraries hinzufügen! der ordner lib sollt auf gleicher ebene sein wie der ordner "src")
     JFontChooser fontChooser = new JFontChooser();
     int result = fontChooser.showDialog(parent);
     if (result == JFontChooser.OK_OPTION)
     {
     Font font = fontChooser.getSelectedFont(); 
     System.out.println("Selected Font : " + font); 
     }
    
     */

    private final String name;
    private final Color foregroundColor, backgroundColor;
    private final Font font;
    private final String DELIMITER = "#";

    public Profile(String name, Color foregroundColor, Color backgroundColor, Font font) {
        this.name = name;
        this.foregroundColor = foregroundColor;
        this.backgroundColor = backgroundColor;
        this.font = font;
    }

    public Profile(String line) throws Exception {

        String[] token = line.split(DELIMITER);
        name = token[0];
        String color = token[1];
        //foregroundColor = token[2]; // ka wie ma des rauslesen
        //backgroundColor = Color (token[3]); //same here
        String fontName = token[4];
        String fontFormat = token[5];
        int fontSize = Integer.parseInt(token[6]);
        // nur wegen dem initalisieren da
        foregroundColor = new Color(123);
        backgroundColor = new Color(123);
        font = new Font("Jokerman", Font.PLAIN, 35);
    }

    public String getName() {
        return name;
    }

    public Color getForegroundColor() {
        return foregroundColor;
    }

    public String getForegroundColorAsString() {
        return foregroundColor.toString();
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public String getBackgroundColorAsString() {
        return backgroundColor.toString();
    }

    public Font getFont() {
        return font;
    }

    public String getFontAsString() {
        return font.getName();
    }

    public int getStyleAsString() {
        return font.getStyle();
    }

    public int getSizeAsString() {
        return font.getSize();
    }

    public void writeTo(BufferedWriter bw) throws IOException {
        /*
         Line Example
         Profile1#green#black#Calibri-PLAIN-12
         Name#fore#back#font#style#size
         */
        bw.write(name);
        bw.write(DELIMITER);

        bw.write(foregroundColor.toString());
        bw.write(DELIMITER);

        bw.write(backgroundColor.toString());
        bw.write(DELIMITER);

        bw.write(font.getName());
        bw.write(DELIMITER);

        bw.write("" + font.getStyle());
        bw.write(DELIMITER);

        bw.write("" + font.getSize());

        bw.newLine();
    }

}
