package at.petritzdesigns.mishell.shell;

import at.petritzdesigns.mishell.helper.Logger;
import at.petritzdesigns.mishell.view.NavigationFilterPrefixWithBackspace;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;
import javax.swing.JTextPane;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ShellExecutor extends Thread {

    private final JTextPane pane;

    public ShellExecutor(String name) {
        super(name);
        this.pane = null;
    }

    public ShellExecutor(String name, JTextPane pane) {
        super(name);
        this.pane = pane;
    }

    @Override
    public void run() {
        Environment.getInstance().applicationStart();

        Scanner scanner = null;
        Interpreter interpreter = null;

        try {
            if (pane == null) {
                scanner = new Scanner(System.in);
                interpreter = new Interpreter(System.out);
            } else {
                scanner = new Scanner(new TextPaneInputStream(pane));
                interpreter = new Interpreter(new TextPaneOutputStream(pane));
            }
            
            //clear at beginning
            interpreter.execute("clear");

            while (Environment.getInstance().isRunning()) {
                String shellPrefix = Environment.getInstance().getShellPrefix();
                interpreter.output(shellPrefix);

                if (pane != null) {
                    updatePanePrefix(pane, pane.getStyledDocument().getLength());
                }

                String line = scanner.nextLine();
                if(Environment.getInstance().getNeedsUpdate()) {
                    interpreter.output("Shell needs update.\n");
                    Environment.getInstance().shellUpdated();
                }
                else {
                    interpreter.execute(line);
                }
            }
            
            if(pane != null) {
                pane.setEditable(false);
            } else {
                System.exit(0);
            }
        } catch (Exception ex) {
            Logger.getDefault().log(ex, true);
        }
    }

    private void updatePanePrefix(JTextPane pane, int length) {
        pane.setNavigationFilter(new NavigationFilterPrefixWithBackspace(length, pane));
    }
}
