package at.petritzdesigns.mishell.shell;

import at.petritzdesigns.mishell.helper.LogType;
import at.petritzdesigns.mishell.helper.Logger;
import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import javax.swing.JTextPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class TextPaneOutputStream extends OutputStream {

    private final JTextPane pane;
    private final MutableAttributeSet keyWord;
    private final StyledDocument doc;

    public TextPaneOutputStream(JTextPane pane) {
        this.pane = pane;
        this.keyWord = new SimpleAttributeSet();
        this.doc = pane.getStyledDocument();
        Environment.getInstance().defaultAttribtutes(keyWord);
    }

    @Override
    public void write(int b) throws IOException {
        try {
            doc.insertString(doc.getLength(), String.valueOf((char) b), keyWord);
            pane.setCaretPosition(doc.getLength());
        } catch (Exception ex) {
            Logger.getDefault().log(LogType.LOG_WARNING, ex, true);
        }
    }

    @Override
    public void write(byte[] b) throws IOException {
        try {
            ColorParser parser = new ColorParser(new String(b), keyWord, doc);
            parser.execute();
            pane.setCaretPosition(doc.getLength());
        } catch (Exception ex) {
            Logger.getDefault().log(LogType.LOG_WARNING, ex, true);
        }
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if ((off < 0) || (off > b.length) || (len < 0)
                || ((off + len) > b.length) || ((off + len) < 0)) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return;
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            sb.append((char) b[off + i]);
        }
        write(sb.toString().getBytes());
    }

}
