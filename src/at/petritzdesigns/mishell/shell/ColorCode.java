package at.petritzdesigns.mishell.shell;

import at.petritzdesigns.mishell.helper.ColorHelper;
import java.awt.Color;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public enum ColorCode {

    RESET(0),
    BOLD(1),
    DIM(2),
    UNDERLINED(4),
    BLINK(5),
    REVERSE(7),
    HIDDEN(8),
    RESET_BOLD(21),
    FG_DEFAULT(39),
    FG_BLACK(30),
    FG_RED(31),
    FG_GREEN(32),
    FG_YELLOW(33),
    FG_BLUE(34),
    FG_MAGENTA(35),
    FG_CYAN(36),
    FG_LIGHT_GRAY(37),
    FG_DARK_GRAY(90),
    FG_LIGHT_RED(91),
    FG_LIGHT_GREEN(92),
    FG_LIGHT_YELLOW(93),
    FG_LIGHT_BLUE(94),
    FG_LIGHT_MAGENTA(95),
    FG_LIGHT_CYAN(96),
    FG_WHITE(97),
    BG_RED(41),
    BG_GREEN(42),
    BG_YELLOW(43),
    BG_BLUE(44),
    BG_MAGENTA(45),
    BG_CYAN(46),
    BG_GRAY(47),
    BG_DEFAULT(49),
    BG_LIGHT_RED(101),
    BG_LIGHT_GREEN(102),
    BG_LIGHT_YELLOW(103),
    BG_LIGHT_BLUE(104),
    BG_LIGHT_MAGENTA(105),
    BG_LIGHT_CYAN(106),
    BG_LIGHT_WHITE(107);

    private final Integer code;
    private static final Pattern TOKEN_REGEX = Pattern.compile("(\\[)([0-9]{1,})(m)");

    private ColorCode(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
    
    /**
     * Return Enum but only if code is valid, else null
     * @param code
     * @return enum
     */
    public static ColorCode getEnum(Integer code) {
        for(ColorCode color : values()) {
            if(color.getCode().equals(code)) {
                return color;
            }
        }
        return null;
    }

    /**
     * Returns Enum but only if valid, else null
     * @param code code to analyse
     * @return enum
     */
    public static ColorCode getEnum(String code) {
        if(code.length() < 4) {
            return null;
        }
        
        if(code.charAt(0) != 3) {
            return null;
        }
        
        Matcher m = TOKEN_REGEX.matcher(code);
        if(m.find()) {
            return getEnum(Integer.parseInt(m.group(2)));
        }
        return null;
    }

    /**
     * Enum to AWT Color Returns null if ColorCode is not a valid
     *
     * @return color
     */
    public Color toColor() {
        switch (this) {
            case FG_BLACK:
                return ColorHelper.stain(Color.black, 0.2f);
            case FG_RED:
                return ColorHelper.stain(Color.red, 0.2f);
            case FG_GREEN:
                return ColorHelper.stain(Color.green, 0.2f);
            case FG_YELLOW:
                return ColorHelper.stain(Color.yellow, 0.2f);
            case FG_BLUE:
                return ColorHelper.stain(Color.blue, 0.2f);
            case FG_MAGENTA:
                return ColorHelper.stain(Color.magenta, 0.2f);
            case FG_CYAN:
                return ColorHelper.stain(Color.cyan, 0.2f);
            case FG_LIGHT_GRAY:
                return Color.lightGray;
            case FG_DARK_GRAY:
                return Color.darkGray;
            case FG_LIGHT_RED:
                return ColorHelper.bleach(Color.red, 0.2f);
            case FG_LIGHT_GREEN:
                return ColorHelper.bleach(Color.green, 0.2f);
            case FG_LIGHT_YELLOW:
                return ColorHelper.bleach(Color.yellow, 0.2f);
            case FG_LIGHT_BLUE:
                return ColorHelper.bleach(Color.blue, 0.2f);
            case FG_LIGHT_MAGENTA:
                return ColorHelper.bleach(Color.magenta, 0.2f);
            case FG_LIGHT_CYAN:
                return ColorHelper.bleach(Color.cyan, 0.2f);
            case FG_WHITE:
                return Color.white;
            case BG_RED:
                return Color.red;
            case BG_GREEN:
                return Color.green;
            case BG_YELLOW:
                return Color.yellow;
            case BG_BLUE:
                return Color.blue;
            case BG_MAGENTA:
                return Color.magenta;
            case BG_CYAN:
                return Color.cyan;
            case BG_GRAY:
                return Color.gray;
            case BG_LIGHT_RED:
                return ColorHelper.bleach(Color.red, 0.2f);
            case BG_LIGHT_GREEN:
                return ColorHelper.bleach(Color.green, 0.2f);
            case BG_LIGHT_YELLOW:
                return ColorHelper.bleach(Color.yellow, 0.2f);
            case BG_LIGHT_BLUE:
                return ColorHelper.bleach(Color.blue, 0.2f);
            case BG_LIGHT_MAGENTA:
                return ColorHelper.bleach(Color.magenta, 0.2f);
            case BG_LIGHT_CYAN:
                return ColorHelper.bleach(Color.cyan, 0.2f);
            case BG_LIGHT_WHITE:
                return ColorHelper.bleach(Color.white, 0.2f);
            default:
                return null;
        }
    }
    
    public ColorCodeType getColorType(){
        String name = name();
        if(name.startsWith("FG")) {
            return ColorCodeType.FOREGROUND;
        }
        else if(name.startsWith("BG")) {
            return ColorCodeType.BACKGROUND;
        }
        else if(name.contains("DEFAULT")) {
            return ColorCodeType.DEFAULT;
        }
        else if(name.contains("RESET")) {
            return ColorCodeType.RESET;
        }
        else {
            return ColorCodeType.ATTRIBUTE;
        }
    } 

    @Override
    public String toString() {
        return String.format("\003[%dm", this.code);
    }
}
