package at.petritzdesigns.mishell.shell;

import at.petritzdesigns.mishell.helper.LogType;
import at.petritzdesigns.mishell.helper.Logger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.JTextPane;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class Shell {

    /**
     * Shell Interpreter class
     */
    private JTextPane pane;
    
    public Shell() {
        this.pane = null;
        Logger.getDefault().log(LogType.LOG_INFO, "Shell initialised...", false);
    }

    /**
     * Run Method (CLI Mode, Sout & Sin)
     */
    public void run() {
        ShellExecutor thread = new ShellExecutor("Shell Thread", this.pane);
        ExecutorService service = Executors.newFixedThreadPool(8);
        service.execute(thread);
    }

    /**
     * Run Method (GUI Mode, TextPane)
     *
     * @param pane
     */
    public void run(JTextPane pane) {
        this.pane = pane;
        this.run();
    }
    
}
