package at.petritzdesigns.mishell.shell.gui;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * MiShell
 * 
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ShellCommandDialog extends javax.swing.JDialog {

    private String title;
    private final ShellCommandPanel panel;
    private ActionListener okListener;
    private ActionListener cancelListener;
    
    public ShellCommandDialog(java.awt.Frame parent, String title, ShellCommandPanel panel) {
        super(parent);
        this.title = title;
        this.panel = panel;
        this.okListener = null;
        this.cancelListener = null;
    }
    
    private void setup() {
        updateTitle();
    }

    public void setWindowTitle(String title) {
        this.title = title;
        updateTitle();
    }
    
    private void updateTitle() {
        super.setTitle(title);
        lbTitle.setText(title);
    }
    
        public void setOkListener(ActionListener listener) {
            this.okListener = listener;
        }

        public void setCancelListener(ActionListener listener) {
            this.cancelListener = listener;
        }
 
    public static void showDialog(ShellCommandDialog dialog) {
        dialog.initComponents();
        dialog.pnData.add(dialog.panel);
        dialog.setMinimumSize(new Dimension((int) dialog.panel.getMinimumSize().getWidth() + 200, 
                (int) dialog.panel.getMinimumSize().getHeight() + 200));
        dialog.setup();
        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                dialog.onClose(null);
            }
        });
        dialog.setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnFooter = new javax.swing.JPanel();
        btOk = new javax.swing.JButton();
        btClose = new javax.swing.JButton();
        pnHeader = new javax.swing.JPanel();
        lbTitle = new javax.swing.JLabel();
        pnData = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(400, 280));

        pnFooter.setLayout(new java.awt.GridBagLayout());

        btOk.setText("OK");
        btOk.setPreferredSize(new java.awt.Dimension(150, 29));
        btOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onOk(evt);
            }
        });
        pnFooter.add(btOk, new java.awt.GridBagConstraints());

        btClose.setText("Close");
        btClose.setPreferredSize(new java.awt.Dimension(150, 29));
        btClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onClose(evt);
            }
        });
        pnFooter.add(btClose, new java.awt.GridBagConstraints());

        getContentPane().add(pnFooter, java.awt.BorderLayout.SOUTH);

        pnHeader.setLayout(new java.awt.GridLayout());

        lbTitle.setFont(new java.awt.Font("Open Sans", 0, 24)); // NOI18N
        lbTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTitle.setText("Command");
        pnHeader.add(lbTitle);

        getContentPane().add(pnHeader, java.awt.BorderLayout.PAGE_START);

        pnData.setLayout(new java.awt.BorderLayout());
        getContentPane().add(pnData, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void onOk(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onOk
        //call handler
        if(okListener != null) {
            okListener.actionPerformed(evt);
        }
        this.dispose();
    }//GEN-LAST:event_onOk

    private void onClose(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onClose
        if(cancelListener != null) {
            cancelListener.actionPerformed(evt);
        }
        this.dispose();
    }//GEN-LAST:event_onClose

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btClose;
    private javax.swing.JButton btOk;
    private javax.swing.JLabel lbTitle;
    private javax.swing.JPanel pnData;
    private javax.swing.JPanel pnFooter;
    private javax.swing.JPanel pnHeader;
    // End of variables declaration//GEN-END:variables

}
