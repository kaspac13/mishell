package at.petritzdesigns.mishell.shell.gui;

import at.petritzdesigns.mishell.shell.commands.ShellCommand;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public abstract class ShellCommandPanel extends JPanel {

    protected ShellCommand command;

    public void setCommand(ShellCommand command) {
        this.command = command;
    }
    
    public abstract ActionListener getOkListener();
    public abstract ActionListener getCancelListener();
}
