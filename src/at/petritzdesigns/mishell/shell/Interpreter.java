package at.petritzdesigns.mishell.shell;

import at.petritzdesigns.mishell.shell.commands.ChangeDirectoryCommand;
import at.petritzdesigns.mishell.shell.commands.ClearCommand;
import at.petritzdesigns.mishell.shell.commands.ListAllCommand;
import at.petritzdesigns.mishell.shell.commands.ListDirectoryCommand;
import at.petritzdesigns.mishell.shell.commands.ListFilesCommand;
import at.petritzdesigns.mishell.shell.commands.QuitCommand;
import at.petritzdesigns.mishell.shell.commands.ShellCommand;
import at.petritzdesigns.mishell.shell.commands.gui.ListAllCommandPanel;
import at.petritzdesigns.mishell.shell.exceptions.LineIsEmptyException;
import at.petritzdesigns.mishell.shell.exceptions.ShellException;
import at.petritzdesigns.mishell.shell.exceptions.UnknownCommandException;
import at.petritzdesigns.mishell.shell.gui.ShellCommandDialog;
import at.petritzdesigns.mishell.shell.gui.ShellCommandPanel;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import javax.swing.JFrame;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class Interpreter {

    private final PrintStream out;
    private final Map<String, ShellCommand> commands;
    private final Map<ShellCommand, ShellCommandPanel> commandViews;

    public Interpreter(OutputStream out) {
        this.out = new PrintStream(out);
        this.commands = new HashMap<>();
        this.commandViews = new HashMap<>();
        setupCommands();
    }

    public synchronized void execute(String line) {
        try {
            if (line.isEmpty()) {
                throw new LineIsEmptyException();
            }

            Scanner scanner = new Scanner(line);
            List<String> parameters = new LinkedList<>();

            while (scanner.hasNext()) {
                parameters.add(scanner.next());
            }

            String command_name = parameters.get(0).toLowerCase();

            if (commands.containsKey(command_name)) {
                parameters.remove(0);

                ShellCommand cmd = commands.get(command_name);
                if (isGuiMode(cmd, parameters)) {
                    execView(cmd, commandViews.get(cmd));
                } else {
                    commands.get(command_name).execute(out, parameters);
                }
            } else {
                boolean found = false;
                for (Map.Entry<String, ShellCommand> entry : commands.entrySet()) {
                    for (String cmd_name : entry.getValue().getAlternatives()) {
                        if (command_name.equals(cmd_name)) {
                            found = true;
                            parameters.remove(0);
                            if (isGuiMode(entry.getValue(), parameters)) {
                                execView(entry.getValue(), commandViews.get(entry.getValue()));
                            } else {
                                entry.getValue().execute(out, parameters);
                            }
                        }
                    }
                }
                if (!found) {
                    try {
                        execCommand(parameters.toArray(new String[0]));
                    } catch (IOException ex) {
                        throw new UnknownCommandException(command_name);
                    }
                }
            }
        } catch (ShellException ex) {
            StringBuilder sb = new StringBuilder();
            sb.append(ColorCode.FG_RED);
            sb.append(ColorCode.BOLD);
            sb.append("Command error: ");
            sb.append(ColorCode.RESET_BOLD);
            sb.append(ex.getMessage());
            sb.append(ColorCode.RESET);
            out.println(sb.toString());
        } catch (Exception ex) {
            StringBuilder sb = new StringBuilder();
            sb.append(ColorCode.FG_RED);
            sb.append(ColorCode.BOLD);
            sb.append("Error: ");
            sb.append(ColorCode.RESET_BOLD);
            sb.append(ex);
            sb.append(ColorCode.RESET);
            out.println(sb.toString());
        } finally {
            out.flush();
        }
    }

    public synchronized void output(String text) throws IOException {
        out.print(text);
        out.flush();
    }

    private void execCommand(String[] cmds) throws IOException {
        Runtime rt = Runtime.getRuntime();
        Process proc = rt.exec(cmds);

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

        String s = null;
        while ((s = stdInput.readLine()) != null) {
            out.println(s);
        }

        while ((s = stdError.readLine()) != null) {
            out.println(s);
        }
    }

    private void execView(ShellCommand cmd, ShellCommandPanel panel) {
        ShellCommandDialog dialog = new ShellCommandDialog(new JFrame(), cmd.getDescription(), panel);
        dialog.setOkListener(panel.getOkListener());
        dialog.setCancelListener(panel.getCancelListener());
        ShellCommandDialog.showDialog(dialog);
    }

    private void setupCommands() {
        registerCommand(new ChangeDirectoryCommand());
        registerCommand(new ClearCommand());
        registerCommand(new ListAllCommand(), new ListAllCommandPanel());
        registerCommand(new ListDirectoryCommand());
        registerCommand(new ListFilesCommand());
        registerCommand(new QuitCommand());
    }

    private void registerCommand(ShellCommand cmd) {
        commands.put(cmd.getName().toLowerCase(), cmd);
    }

    private void registerCommand(ShellCommand cmd, ShellCommandPanel gui) {
        registerCommand(cmd);
        gui.setCommand(cmd);
        commandViews.put(cmd, gui);
    }

    private boolean isGuiMode(ShellCommand cmd, List<String> parameters) {
        if (!commandViews.containsKey(cmd)) {
            return false;
        }
        return parameters.contains("-g");
    }
}
