package at.petritzdesigns.mishell.shell;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public enum ColorCodeType {
    FOREGROUND,
    BACKGROUND,
    DEFAULT,
    RESET,
    ATTRIBUTE;
}
