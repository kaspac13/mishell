package at.petritzdesigns.mishell.shell;

import at.petritzdesigns.mishell.helper.Logger;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ArrayBlockingQueue;
import javax.swing.JTextPane;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class TextPaneInputStream extends InputStream implements KeyListener {

    private final JTextPane pane;
    private final ArrayBlockingQueue<Integer> queue;

    public TextPaneInputStream(JTextPane pane) {
        this.pane = pane;
        this.queue = new ArrayBlockingQueue<>(1024);
        this.pane.addKeyListener(this);
    }

    /*
     * Taken from 
     * http://stackoverflow.com/a/28519681/3575969
     */
    @Override
    public int read() throws IOException {
        Integer i = null;
        try {
            i = queue.take();
        } catch (InterruptedException ex) {
            Logger.getDefault().log(ex, true);
        }

        if (i != null) {
            return i;
        }

        return -1;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }
        int c = read();
        if (c == -1) {
            return -1;
        }
        b[off] = (byte) c;

        int i = 1;
        try {
            for (; i < len && available() > 0; i++) {
                c = read();
                if (c == -1) {
                    break;
                }
                b[off + i] = (byte) c;
            }
        } catch (IOException ex) {
            Logger.getDefault().log(ex, true);
        }
        return i;
    }

    @Override
    public int available() throws IOException {
        return queue.size();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        int c = e.getKeyChar();
        try {
            queue.put(c);
        } catch (InterruptedException ex) {
            Logger.getDefault().log(ex, true);
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
