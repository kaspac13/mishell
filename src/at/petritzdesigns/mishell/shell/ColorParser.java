package at.petritzdesigns.mishell.shell;

import at.petritzdesigns.mishell.helper.ColorHelper;
import java.awt.Color;
import java.util.LinkedList;
import java.util.List;
import javax.swing.text.BadLocationException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ColorParser {

    /*
     Example:
     \033[41m (background: red)
     same as bash shell color codes
     */
    private final String str;
    private final MutableAttributeSet defaultKeyWord;
    private final StyledDocument doc;

    public ColorParser(String str, MutableAttributeSet defaultKeyWord, StyledDocument doc) {
        this.str = str;
        this.defaultKeyWord = defaultKeyWord;
        this.doc = doc;
    }

    public void execute() throws BadLocationException {
        //Regex: (\\033\[[0-9]{1,}\m)        
        if (str.length() >= 4) {
            Object[] textAndColors = parse().toArray();
            MutableAttributeSet keyWord = new SimpleAttributeSet();

            for (Object textAndColor : textAndColors) {
                if (textAndColor instanceof ColorCode) {
                    ColorCode color = (ColorCode) textAndColor;
                    switch (color.getColorType()) {
                        case ATTRIBUTE:
                            switch (color) {
                                case BOLD:
                                    StyleConstants.setBold(keyWord, true);
                                    break;
                                case UNDERLINED:
                                    StyleConstants.setUnderline(keyWord, true);
                                    break;
                                case DIM:
                                    Color oldColorFG = StyleConstants.getForeground(keyWord);
                                    Color oldColorBG = StyleConstants.getBackground(keyWord);
                                    StyleConstants.setForeground(keyWord, ColorHelper.stain(oldColorFG, 0.2f));
                                    StyleConstants.setBackground(keyWord, ColorHelper.stain(oldColorBG, 0.2f));
                                    break;
                                case HIDDEN:
                                    StyleConstants.setForeground(keyWord, StyleConstants.getBackground(keyWord));
                                    break;
                            }
                            break;
                        case BACKGROUND:
                            StyleConstants.setBackground(keyWord, color.toColor());
                            break;
                        case DEFAULT:
                            if (color == ColorCode.BG_DEFAULT) {
                                StyleConstants.setBackground(keyWord, StyleConstants.getBackground(defaultKeyWord));
                            }
                            if (color == ColorCode.FG_DEFAULT) {
                                StyleConstants.setForeground(keyWord, StyleConstants.getForeground(defaultKeyWord));
                            }
                            break;
                        case FOREGROUND:
                            StyleConstants.setForeground(keyWord, color.toColor());
                            break;
                        case RESET:
                            if (color == ColorCode.RESET) {
                                Environment.getInstance().defaultAttribtutes(keyWord);
                            }
                            if (color == ColorCode.RESET_BOLD) {
                                StyleConstants.setBold(keyWord, false);
                            }
                            break;
                        default:
                            break;
                    }
                } else {
                    doc.insertString(doc.getLength(), textAndColor.toString(), keyWord);
                }
            }
        } else {
            doc.insertString(doc.getLength(), str, defaultKeyWord);
        }
    }

    private List<Object> parse() {
        List<Object> list = new LinkedList<>();
        StringBuilder buffer = new StringBuilder();
        StringBuilder buffer2 = new StringBuilder();

        for (Character c : str.toCharArray()) {
            if (buffer.length() == 0 && c != 3) {
                buffer2.append(c);
                buffer = new StringBuilder();
            } else if (c == 3) {
                list.add(buffer2.toString());
                buffer2 = new StringBuilder();
                buffer.append(c);
            } else {
                buffer.append(c);
            }

            ColorCode parsed = ColorCode.getEnum(buffer.toString());
            if (parsed != null) {
                list.add(parsed);
                buffer = new StringBuilder();
            }
        }

        list.add(buffer2.toString());
        list.add(buffer.toString());
        return list;
    }

}
