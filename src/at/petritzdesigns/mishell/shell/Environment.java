package at.petritzdesigns.mishell.shell;

import at.petritzdesigns.mishell.helper.TimeInterval;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.StyleConstants;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class Environment {

    private static Environment instance;

    private Boolean running;
    private Boolean needsUpdate;
    private TimeInterval start;
    private TimeInterval end;
    
    private File currentDir;
    
    private Environment() {
        this.start = null;
        this.end = null;
        this.running = false;
        this.needsUpdate = false;
        this.currentDir = new File(System.getProperty("user.home"));
    }

    public static Environment getInstance() {
        if (instance == null) {
            instance = new Environment();
        }
        return instance;
    }

    /**
     * If the Shell is running
     *
     * @return
     */
    public Boolean isRunning() {
        return running;
    }

    public void setCurrentDirectory(File dir) {
        this.currentDir = dir;
    }

    public File getCurrentDirectory() throws IOException {
        return currentDir.getCanonicalFile();
    }

    /**
     * This method should be called when the application is about to start
     */
    public void applicationStart() {
        //starts to stop execution time
        start = new TimeInterval();
        end = null;
        running = true;
    }

    /**
     * This method should be called when the application is about to quit
     */
    public void applicationEnd() {
        //stops exection time
        end = new TimeInterval();
        running = false;
    }

    /**
     * Execution Time in milliseconds
     *
     * @return execution time
     */
    public Long getExecutionTime() {
        if (start == null || end == null) {
            return 0L;
        }
        return start.calcDifference(end);
    }
    
    public void defaultAttribtutes(MutableAttributeSet keyWord) {
        //TODO Profile Integration
        // Na
        StyleConstants.setForeground(keyWord, Color.black);
        StyleConstants.setBackground(keyWord, Color.white);
    }
    
    public void shellNeedsUpdate() {
        this.needsUpdate = true;
    }
    
    public void shellUpdated() {
        this.needsUpdate = false;
    }

    public Boolean getNeedsUpdate() {
        return needsUpdate;
    }
    
    public String determineHostname() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostName();
    }

    public String determineUsername() {
        return System.getProperty("user.name");
    }

    public String getShellPrefix() throws UnknownHostException, IOException {
        StringBuilder sb = new StringBuilder();
        
        sb.append(ColorCode.FG_CYAN);
        sb.append(determineHostname());
        sb.append(ColorCode.RESET);
        sb.append("@");
        sb.append(ColorCode.FG_LIGHT_RED);
        sb.append(determineUsername());
        sb.append(":");
        sb.append(ColorCode.RESET);
        sb.append(ColorCode.FG_BLUE);
        sb.append(Environment.getInstance().getCurrentDirectory().getName());
        sb.append("/");
        sb.append(ColorCode.RESET);
        sb.append("# ");
        
        return sb.toString();
    }

}
