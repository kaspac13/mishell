package at.petritzdesigns.mishell.shell.commands;

import java.io.PrintStream;
import java.util.List;

/**
 * MiShell
 * 
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ClearCommand extends ShellCommand {

    public ClearCommand() {
        super("clear", ShellCommandParameters.NO_PARAMS);
    }

    @Override
    void handle(PrintStream out, List<String> params) throws Exception {
        for(int i = 0; i < 100; i++) {
            out.println();
        }
        out.flush();
    }
    
}
