package at.petritzdesigns.mishell.shell.commands;

import at.petritzdesigns.mishell.shell.ColorCode;
import at.petritzdesigns.mishell.shell.Environment;
import java.io.PrintStream;
import java.util.List;

/**
 * MiShell
 * 
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class QuitCommand extends ShellCommand {

    public QuitCommand() {
        super("quit", ShellCommandParameters.NO_PARAMS, "exit");
    }

    @Override
    void handle(PrintStream out, List<String> params) throws Exception {
        out.println("Exiting shell...");
        
        Environment.getInstance().applicationEnd();
        
        //print stats
        Long seconds = Environment.getInstance().getExecutionTime() / 1000;
        StringBuilder sb = new StringBuilder();
        sb.append(ColorCode.FG_GREEN);
        sb.append(String.format("Execution time: %dsec", seconds));
        sb.append(ColorCode.RESET);
        
        out.println(sb.toString());
        out.flush();
    }
    
}
