package at.petritzdesigns.mishell.shell.commands.gui;

import at.petritzdesigns.mishell.helper.Logger;
import at.petritzdesigns.mishell.shell.Environment;
import at.petritzdesigns.mishell.shell.commands.data.ListFile;
import at.petritzdesigns.mishell.shell.commands.model.ListAllCommandModel;
import at.petritzdesigns.mishell.shell.gui.ShellCommandPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ListAllCommandPanel extends ShellCommandPanel {

    /*
     TODO:
     Implement Colors for Cells
     */
    private final ListAllCommandModel model;
    private File directory;

    public ListAllCommandPanel() {
        initComponents();
        this.model = new ListAllCommandModel();
        this.taList.setModel(model);
        try {
            directory = Environment.getInstance().getCurrentDirectory();
            model.list();
            tfPath.setText(directory.getCanonicalPath());
        } catch (IOException ex) {
            Logger.getDefault().log(ex, true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        pnActions = new javax.swing.JPanel();
        btRefresh = new javax.swing.JButton();
        btBack = new javax.swing.JButton();
        spMain = new javax.swing.JScrollPane();
        taList = new javax.swing.JTable()
        ;
        pnHeader = new javax.swing.JPanel();
        tfPath = new javax.swing.JTextField();

        setMinimumSize(new java.awt.Dimension(286, 179));
        setLayout(new java.awt.BorderLayout());

        pnActions.setLayout(new java.awt.GridBagLayout());

        btRefresh.setText("Refresh");
        btRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onRefresh(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnActions.add(btRefresh, gridBagConstraints);

        btBack.setText("Back");
        btBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onBack(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnActions.add(btBack, gridBagConstraints);

        add(pnActions, java.awt.BorderLayout.PAGE_END);

        taList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        taList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onClicked(evt);
            }
        });
        spMain.setViewportView(taList);

        add(spMain, java.awt.BorderLayout.CENTER);

        pnHeader.setLayout(new java.awt.BorderLayout());

        tfPath.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onPath(evt);
            }
        });
        pnHeader.add(tfPath, java.awt.BorderLayout.CENTER);

        add(pnHeader, java.awt.BorderLayout.PAGE_START);
    }// </editor-fold>//GEN-END:initComponents

    private void onRefresh(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onRefresh
        try {
            model.list();
        } catch (IOException ex) {
            Logger.getDefault().log(ex, true);
        }
    }//GEN-LAST:event_onRefresh

    private void onClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onClicked
        try {
            int index = taList.getSelectedRow();
            if (index == -1) {
                throw new Exception("No row selected");
            }
            ListFile file = model.get(index);
            if (SwingUtilities.isRightMouseButton(evt)) {
                showInformation(file);
            } else if (evt.getClickCount() == 2) {
                switchDirectory(file);
            }
        } catch (Exception ex) {
            Logger.getDefault().log(ex, true);
        }
    }//GEN-LAST:event_onClicked

    private void onBack(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onBack
        try {
            File currentDir = Environment.getInstance().getCurrentDirectory();
            File dir = new File(currentDir.getCanonicalPath() + "/" + "../");
            update(dir);
        } catch (IOException ex) {
            Logger.getDefault().log(ex, true);
        }
    }//GEN-LAST:event_onBack

    private void onPath(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onPath
        try {
            String path = tfPath.getText().trim();
            if (path.isEmpty()) {
                tfPath.setText(Environment.getInstance().getCurrentDirectory().getCanonicalPath());
                throw new Exception("Invalid path");
            }

            File dir = new File(path);
            if (dir.isDirectory() && dir.isAbsolute() && dir.canRead()) {
                update(dir);
            } else {
                File dir2 = new File(dir.getCanonicalPath() + "/" + path);
                if (dir2.isDirectory() && dir2.canRead()) {
                    update(dir2);
                } else {
                    throw new Exception("File is not a directory");
                }
            }
        } catch (Exception ex) {
            Logger.getDefault().log(ex, true);
        }
    }//GEN-LAST:event_onPath

    private void showInformation(ListFile file) throws IOException {
        JOptionPane.showMessageDialog(this, file.getInformationString());
    }

    private void switchDirectory(ListFile file) throws Exception {
        File currentDir = Environment.getInstance().getCurrentDirectory();
        File nDir = file.getFile();
        if (nDir.isAbsolute() && nDir.canRead() && nDir.isDirectory()) {
            update(nDir);
        } else {
            File dir = new File(currentDir.getCanonicalPath() + "/" + file.getFileName());
            if (dir.isDirectory() && dir.canRead()) {
                update(dir);
            } else {
                throw new Exception("File is not a directory");
            }
        }
    }

    private void update(File dir) throws IOException {
        Environment.getInstance().setCurrentDirectory(dir);
        Environment.getInstance().shellNeedsUpdate();
        model.list();
        tfPath.setText(dir.getCanonicalPath());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBack;
    private javax.swing.JButton btRefresh;
    private javax.swing.JPanel pnActions;
    private javax.swing.JPanel pnHeader;
    private javax.swing.JScrollPane spMain;
    private javax.swing.JTable taList;
    private javax.swing.JTextField tfPath;
    // End of variables declaration//GEN-END:variables

    //Hidden features
    @Override
    public ActionListener getOkListener() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //everything fine, dont change to old directory
                Environment.getInstance().shellNeedsUpdate();
            }
        };
    }

    @Override
    public ActionListener getCancelListener() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //change to old directory
                Environment.getInstance().setCurrentDirectory(directory);
                Environment.getInstance().shellNeedsUpdate();
            }
        };
    }

}
