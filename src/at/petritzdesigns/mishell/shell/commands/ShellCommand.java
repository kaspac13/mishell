package at.petritzdesigns.mishell.shell.commands;

import at.petritzdesigns.mishell.shell.exceptions.WrongParameterCountException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public abstract class ShellCommand {
    
    private final String commandName;
    private String description;
    private final List<String> alternatives;
    private final Integer parameterCount;

    public ShellCommand(String command_name, Integer parameter_count) {
        this.commandName = command_name;
        this.parameterCount = parameter_count;
        this.alternatives = new LinkedList<>();
        this.description = null;
    }
    
    public ShellCommand(String command_name, ShellCommandParameters parameter_count) {
        this.commandName = command_name;
        this.parameterCount = parameter_count.getValue();
        this.alternatives = new LinkedList<>();
        this.description = null;
    }
    
    public ShellCommand(String command_name, Integer parameter_count, String... alternatives) {
        this.commandName = command_name;
        this.parameterCount = parameter_count;
        this.alternatives = Arrays.asList(alternatives);
        this.description = null;
    }
    
    public ShellCommand(String command_name, ShellCommandParameters parameter_count, String... alternatives) {
        this.commandName = command_name;
        this.parameterCount = parameter_count.getValue();
        this.alternatives = Arrays.asList(alternatives);
        this.description = null;
    }
    
    /**
     * This method is called wenn the command is executet
     * @param out Stream to write the output
     * @param params Parameters for the command
     * @throws Exception 
     */
    abstract void handle(PrintStream out, List<String> params) throws Exception;
    
    private boolean checkParameterCount(Integer count) {
        if(parameterCount == Integer.MAX_VALUE) {
            return true;
        }
        if(parameterCount >= 0) {
            return parameterCount.equals(count);
        }
        if(parameterCount < 0) {
            return count >= -parameterCount;
        }
        return false;
    }
    
    public void execute(PrintStream out, List<String> params) throws Exception {
        if(checkParameterCount(params.size())) {
            handle(out, params);
        }
        else {
            throw new WrongParameterCountException(params.size());
        }
    }

    public String getName() {
        return commandName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description != null ? description : commandName;
    }

    public List<String> getAlternatives() {
        return alternatives;
    }
}