package at.petritzdesigns.mishell.shell.commands;

import at.petritzdesigns.mishell.helper.FileHelper;
import at.petritzdesigns.mishell.shell.ColorCode;
import at.petritzdesigns.mishell.shell.Environment;
import java.io.File;
import java.io.PrintStream;
import java.util.List;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ListDirectoryCommand extends ShellCommand {

    public ListDirectoryCommand() {
        super("ld", ShellCommandParameters.NO_PARAMS);
    }

    @Override
    void handle(PrintStream out, List<String> params) throws Exception {
        /*
         Info:
         If directory is hidden: dim
         If directory is read only: red background
         */
        StringBuilder sb = new StringBuilder();
        File dir = Environment.getInstance().getCurrentDirectory();
        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                if (file.isHidden()) {
                    sb.append(ColorCode.FG_LIGHT_GRAY);
                }
                if (file.canRead() && !file.canWrite()) {
                    sb.append(ColorCode.BG_LIGHT_YELLOW);
                }
                sb.append(FileHelper.getFileSize(file));
                sb.append("\t");
                sb.append(file.getName());
                sb.append("/");
                sb.append(ColorCode.RESET);
                sb.append("\n");
            }
        }
        out.print(sb.toString());
        out.flush();
    }

}
