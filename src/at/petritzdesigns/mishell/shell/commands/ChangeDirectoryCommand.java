package at.petritzdesigns.mishell.shell.commands;

import at.petritzdesigns.mishell.shell.Environment;
import at.petritzdesigns.mishell.shell.exceptions.ShellException;
import java.io.File;
import java.io.PrintStream;
import java.util.List;

/**
 * MiShell
 * 
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ChangeDirectoryCommand extends ShellCommand {

    public ChangeDirectoryCommand() {
        super("cd", ShellCommandParameters.EXACTLY_1);
    }

    @Override
    void handle(PrintStream out, List<String> params) throws Exception {
        File currentDir = Environment.getInstance().getCurrentDirectory();
        
        String sDir = params.get(0);
        File nDir = new File(sDir);
        if(nDir.isAbsolute() && nDir.canRead() && nDir.isDirectory()) {
            Environment.getInstance().setCurrentDirectory(nDir);
        } else {
            File dir = new File(currentDir.getCanonicalPath() + "/" + sDir);
            if(dir.isDirectory() && dir.canRead()) {
               Environment.getInstance().setCurrentDirectory(dir);
            } else {
                throw new ShellException("Not a directory.");
            }  
        }
    }
    
}
