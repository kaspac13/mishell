package at.petritzdesigns.mishell.shell.commands.model;

import at.petritzdesigns.mishell.shell.Environment;
import at.petritzdesigns.mishell.shell.commands.data.ListAllCommandColumnNames;
import at.petritzdesigns.mishell.shell.commands.data.ListFile;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ListAllCommandModel extends AbstractTableModel {

    private final List<ListFile> list;

    public ListAllCommandModel() {
        this.list = new LinkedList<>();
    }
    
    public void add(ListFile file) {
        if(list.add(file)) {
            int index = list.indexOf(file);
            super.fireTableRowsInserted(index, index);
        }
        else {
            //just to make sure
            super.fireTableDataChanged();
        }
    }
    
    public ListFile get(int index) {
        return list.get(index);
    }

    public void list() throws IOException {
        list.clear();
        File dir = Environment.getInstance().getCurrentDirectory();
        for (File file : dir.listFiles()) {
            add(new ListFile(file));
        }
        //again just to make sure
        super.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public String getColumnName(int column) {
        return ListAllCommandColumnNames.values()[column].getName();
    }

    @Override
    public int getColumnCount() {
        return ListAllCommandColumnNames.values().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ListFile file = list.get(rowIndex);
        switch (ListAllCommandColumnNames.values()[columnIndex]) {
            case FILENAME:
                return file.getFileName();
            case FILESIZE:
                return file.getFileSize();
            case LAST_MODIFIED:
                return ListFile.format(file.getLastAccess());
            default:
                return "-";
        }
    }

}
