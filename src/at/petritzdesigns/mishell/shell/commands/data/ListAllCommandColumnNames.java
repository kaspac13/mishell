package at.petritzdesigns.mishell.shell.commands.data;

/**
 * MiShell
 * 
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public enum ListAllCommandColumnNames {
    FILESIZE("Filesize"), 
    FILENAME("Filename"),
    LAST_MODIFIED("Last modified");
    
    private final String name;

    private ListAllCommandColumnNames(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
