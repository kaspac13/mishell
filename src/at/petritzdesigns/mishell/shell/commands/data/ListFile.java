package at.petritzdesigns.mishell.shell.commands.data;

import at.petritzdesigns.mishell.helper.FileHelper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ListFile {

    private final File file;
    BasicFileAttributes attr;
    
    public ListFile(File file) throws IOException {
        this.file = file;
        this.attr = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
    }

    public String getFileName() {
        return file.getName() + (file.isDirectory() ? "/" : "");
    }

    public String getFileSize() {
        return FileHelper.getFileSize(file);
    }

    public Date getLastModified() {
        return new Date(file.lastModified());
    }
    
    public Date getCreated() {
        return new Date(attr.creationTime().toMillis());
    }
    
    public Date getLastAccess() {
        return new Date(attr.lastAccessTime().toMillis());
    }
    
    public String getPath() throws IOException {
        return file.getCanonicalPath();
    }
    
    public String getInformationString() throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<body>");
        
        sb.append("<h1>").append(getFileName()).append("</h1>");
        sb.append("<ul>");
        sb.append("<li>").append("Size: ").append(getFileSize()).append("</li>");
        sb.append("<li>").append("Path: ").append(getPath()).append("</li>");
        sb.append("<li>").append("Created: ").append(format(getCreated())).append("</li>");
        sb.append("<li>").append("Last access: ").append(format(getLastAccess())).append("</li>");
        sb.append("<li>").append("Last modified: ").append(format(getLastModified())).append("</li>");
        sb.append("</ul>");
        
        sb.append("</body>");
        sb.append("</html>");
        return sb.toString();
    }

    public File getFile() {
        return file;
    }
            
    public static String format(Date date) {
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(date);
    }
}
