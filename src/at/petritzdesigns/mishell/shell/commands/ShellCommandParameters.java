package at.petritzdesigns.mishell.shell.commands;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public enum ShellCommandParameters {
    EXACTLY_1(1),
    EXACTLY_2(2),
    EXACTLY_3(3),
    EXACTLY_4(4),
    EXACTLY_5(5),
    EXACTLY_6(6),
    EXACTLY_7(7),
    EXACTLY_8(8),
    EXACTLY_9(9),
    EXACTLY_10(10),
    NO_PARAMS(0),
    UNLIMITED_PARAMS(Integer.MAX_VALUE),
    MORE_THAN_1(-1),
    MORE_THAN_2(-2),
    MORE_THAN_3(-3),
    MORE_THAN_4(-4),
    MORE_THAN_5(-5),
    MORE_THAN_6(-6),
    MORE_THAN_7(-7),
    MORE_THAN_8(-8),
    MORE_THAN_9(-9),
    MORE_THAN_10(-10);
    
    private final Integer num;

    private ShellCommandParameters(Integer num) {
        this.num = num;
    }

    public Integer getValue() {
        return num;
    }

    @Override
    public String toString() {
        return String.format("%s:%d", this.name(), this.getValue());
    }
}
