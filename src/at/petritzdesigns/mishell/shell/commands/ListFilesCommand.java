package at.petritzdesigns.mishell.shell.commands;

import at.petritzdesigns.mishell.helper.FileHelper;
import at.petritzdesigns.mishell.shell.ColorCode;
import at.petritzdesigns.mishell.shell.Environment;
import java.io.File;
import java.io.PrintStream;
import java.util.List;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ListFilesCommand extends ShellCommand {

    public ListFilesCommand() {
        super("lf", ShellCommandParameters.NO_PARAMS);
    }

    @Override
    void handle(PrintStream out, List<String> params) throws Exception {
        /*
         Info:
         If file is hidden: gray
         If the file is read only: light blue background
         If the file is an executable: light magenta background
         */
        StringBuilder sb = new StringBuilder();
        File dir = Environment.getInstance().getCurrentDirectory();
        for (File file : dir.listFiles()) {
            if (file.isFile()) {
                if (file.isHidden()) {
                    sb.append(ColorCode.FG_LIGHT_GRAY);
                }
                if (file.canRead() && !file.canWrite()) {
                    sb.append(ColorCode.BG_LIGHT_BLUE);
                }
                if (file.canExecute()) {
                    sb.append(ColorCode.BG_LIGHT_MAGENTA);
                }
                sb.append(FileHelper.getFileSize(file));
                sb.append("\t");
                sb.append(file.getName());
                sb.append(ColorCode.RESET);
                sb.append("\n");
            }
        }
        out.print(sb.toString());
        out.flush();
    }

}
