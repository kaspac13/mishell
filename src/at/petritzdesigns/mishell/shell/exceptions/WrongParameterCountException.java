package at.petritzdesigns.mishell.shell.exceptions;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class WrongParameterCountException extends ShellException {

    private final String msg;
    
    public WrongParameterCountException(Integer count) {
        msg = "Wrong Parameter Count: " + count;
    }

    public WrongParameterCountException(String msg) {
        this.msg = msg;
    }

    @Override
    public String getMessage() {
        return msg;
    }
}
