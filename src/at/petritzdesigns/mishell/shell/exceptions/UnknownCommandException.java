package at.petritzdesigns.mishell.shell.exceptions;

/**
 * MiShell
 * 
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class UnknownCommandException extends ShellException {

    private final String msg;
    
    public UnknownCommandException() {
        msg = "Unknown Command";
    }

    public UnknownCommandException(String msg) {
        this.msg = String.format("Unknown Command %s", msg);
    }

    @Override
    public String getMessage() {
        return msg;
    }
}
