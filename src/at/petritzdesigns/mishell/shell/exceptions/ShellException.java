package at.petritzdesigns.mishell.shell.exceptions;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ShellException extends Exception {

    public ShellException() {
    }

    public ShellException(String msg) {
        super(msg);
    }

    @Override
    public String getMessage() {
        if(!super.getMessage().isEmpty()) {
            return super.getMessage();
        }
        return super.toString();
    }
}
