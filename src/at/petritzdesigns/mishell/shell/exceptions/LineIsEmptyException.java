package at.petritzdesigns.mishell.shell.exceptions;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class LineIsEmptyException extends ShellException {

    private final String msg;
    
    public LineIsEmptyException() {
        msg = "Command line is empty";
    }

    public LineIsEmptyException(String msg) {
        this.msg = msg;
    }

    @Override
    public String getMessage() {
        return msg;
    }
}
