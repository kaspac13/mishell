package at.petritzdesigns.mishell;

import at.petritzdesigns.mishell.helper.Logger;
import at.petritzdesigns.mishell.shell.Shell;
import at.petritzdesigns.mishell.view.MainWindow;
import java.awt.EventQueue;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class MiShell {

    /**
     * Entry Point of Application
     *
     * @param args -c for cli-mode
     */
    public static void main(String[] args) {
        Shell shell = new Shell();
        
        if (!isCliMode(args)) {
            try {
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        MainWindow mw = new MainWindow(shell);
                        mw.setVisible(true);
                    }
                });
            } catch (Exception ex) {
                Logger.getDefault().log(ex, true);
            }
        }
        else {
            try {
                shell.run();
            }
            catch(Exception ex) {
                Logger.getDefault().log(ex, true);
            }
        }
    }

    private static boolean isCliMode(String[] args) {
        if (args.length == 1) {
            if (args[0].equals("-c")) {
                return true;
            }
        }
        return false;
    }
}
