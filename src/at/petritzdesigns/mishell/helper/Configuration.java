package at.petritzdesigns.mishell.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Configuration {

    private static Configuration instance;
    private Properties properties;
    private Properties defaultConfig;

    private Configuration() {
        try {
            setupDefaultConfig();
            properties = new Properties(defaultConfig);
            createConfigFileIfNotExist();
            properties.load(new FileInputStream(get("config_path")));
        } catch (IOException ex) {
            Logger.getDefault().log(LogType.LOG_IOERROR, ex, true);
        } catch (Exception ex) {
            Logger.getDefault().log(ex, true);
        }
    }

    public static Configuration getDefault() {
        if (instance == null) {
            instance = new Configuration();
        }
        return instance;
    }

    public String get(String key) {
        String val = properties.getProperty(key);
        if (val == null) {
            return "";
        }
        return val;
    }

    public boolean set(String key, String value) {
        try {
            properties.setProperty(key, value);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        properties.store(new FileWriter(get("config_path")), "MiShell");
                    } catch (IOException ex) {
                        Logger.getDefault().log(LogType.LOG_IOERROR, ex, true);
                    } catch (Exception ex) {
                        Logger.getDefault().log(ex, true);
                    }
                }
            }).start();
        } catch (Exception ex) {
            Logger.getDefault().log(LogType.LOG_DEBUG, ex, true);
            return false;
        }

        return true;
    }

    public boolean getBool(String key) {
        String val = get(key);
        if (val.isEmpty()) {
            return false;
        }
        return val.equals("true");
    }

    public boolean set(String key, boolean value) {
        return set(key, value + "");
    }

    public Integer getInt(String key) {
        try {
            return Integer.parseInt(get(key));
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public boolean set(String key, Integer value) {
        return set(key, value + "");
    }

    public boolean delete(String key) {
        try {
            properties.remove(key);
        } catch (Exception ex) {
            Logger.getDefault().log(LogType.LOG_DEBUG, ex, true);
            return false;
        }

        return true;
    }

    private void createConfigFileIfNotExist() throws IOException {
        File path = new File(get("config_path"));

        if (!path.getParentFile().exists()) {
            path.getParentFile().mkdirs();
        }
        if (!path.exists()) {
            path.createNewFile();
        }
    }

    private void setupDefaultConfig() {
        defaultConfig = new Properties();
        defaultConfig.put("should_write_log", true);
        defaultConfig.put("is_debug", true);
        defaultConfig.put("stacktrace", true);

        defaultConfig.put("config_path", "config/default.pdc");
        defaultConfig.put("log_path", "log/default.pdl");
        defaultConfig.put("profiles_path", "profiles/default.pdp");
    }
}
