package at.petritzdesigns.mishell.helper;

import at.petritzdesigns.mishell.shell.TextPaneOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Logger {

    private static Logger instance;
    private JTextPane textPane;
    private BufferedWriter bw;

    private Logger() {
        try {
            createLogFileIfNotExists();
            bw = new BufferedWriter(new FileWriter(Configuration.getDefault().get("log_path"), true));
        } catch (IOException ex) {
            System.err.println("I/O Fehler: " + ex.getLocalizedMessage());
        } catch (Exception ex) {
            System.err.println("Fehler: " + ex.getLocalizedMessage());
        }
    }

    public static Logger getDefault() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    public void setTextPane(JTextPane pane) {
        this.textPane = pane;
    }

    public void log(String text, boolean show) {
        if (show) {
            show(String.format("Ein Fehler ist aufgetreten: %s", text));
        }
        write(text);
    }

    public void log(Exception ex, boolean show) {
        if (show) {
            show(String.format("Ein Fehler ist aufgetreten: %s", ex.getMessage()));
        }
        write(getExceptionAsUsefulString(ex));
    }

    public void log(LogType type, String text, boolean show) {
        if (show) {
            show(String.format("Ein Fehler ist aufgetreten: %s", text));
        }
        write(String.format("%s: %s", getTypeAsString(type), text));
    }

    public void log(LogType type, Exception ex, boolean show) {
        if (show) {
            show(String.format("Ein Fehler ist aufgetreten: %s", ex.getMessage()));
        }
        write(String.format("%s: %s", getTypeAsString(type), getExceptionAsUsefulString(ex)));
    }

    public void deleteLog() {
        File path = new File(Configuration.getDefault().get("log_path"));
        if (path.getParentFile().exists()) {
            path.getParentFile().delete();
        }
        if (path.exists()) {
            path.delete();
        }
    }

    private void createLogFileIfNotExists() throws IOException {
        File path = new File(Configuration.getDefault().get("log_path"));

        if (!path.getParentFile().exists()) {
            path.getParentFile().mkdirs();
        }
        if (!path.exists()) {
            path.createNewFile();
        }
    }

    private void show(String text) {
        if (text == null || text.isEmpty() || text.equals("null")) {
            text = "Unbekannter Fehler";
        }
        JOptionPane.showMessageDialog(null, text);
    }

    private void write(String text) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (Configuration.getDefault().getBool("is_debug")) {
                    System.out.println(text);
                }

                if (Configuration.getDefault().getBool("stacktrace")) {
                    for (StackTraceElement elem : Thread.currentThread().getStackTrace()) {
                        System.err.println(elem);
                    }
                }

                if (Configuration.getDefault().getBool("should_write_log")) {
                    try {
                        createLogFileIfNotExists();
                        bw.write(currentTime() + " ");
                        bw.write(text);
                        bw.newLine();
                        bw.flush();
                    } catch (IOException ex) {
                        System.err.println("I/O Fehler: " + ex.getLocalizedMessage());
                    } catch (Exception ex) {
                        System.err.println("Fehler: " + ex.getLocalizedMessage());
                    }
                }

                if (textPane != null) {
                    /* TODO: different colors */
                    
                    TextPaneOutputStream out = new TextPaneOutputStream(textPane);
                    PrintStream print = new PrintStream(out);
                    print.println(text);
                    print.flush();
                }
            }
        }).start();
    }

    private String currentTime() {
        return new Date().toString();
    }

    private String getExceptionAsUsefulString(Exception ex) {
        StringBuilder sb = new StringBuilder();

        sb.append(ex.getClass().toGenericString());
        sb.append(" --> ");
        sb.append(ex.getMessage());

        return sb.toString();
    }

    private String getTypeAsString(LogType type) {
        return type.getName();
    }
}
