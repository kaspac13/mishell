package at.petritzdesigns.mishell.helper;

import java.awt.Color;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class ColorHelper {

    /*
     Link: http://www.java2s.com/Code/Java/2D-Graphics-GUI/Lightensacolorbyagivenamount.htm
     */
    public static Color bleach(Color color, float amount) {
        int red = (int) ((color.getRed() * (1 - amount) / 255 + amount) * 255);
        int green = (int) ((color.getGreen() * (1 - amount) / 255 + amount) * 255);
        int blue = (int) ((color.getBlue() * (1 - amount) / 255 + amount) * 255);
        return new Color(red, green, blue);
    }

    public static Color stain(Color color, float amount) {
        int red = (int) ((color.getRed() * (1 - amount) / 255) * 255);
        int green = (int) ((color.getGreen() * (1 - amount) / 255) * 255);
        int blue = (int) ((color.getBlue() * (1 - amount) / 255) * 255);
        return new Color(red, green, blue);
    }
}
