package at.petritzdesigns.mishell.helper;

import java.io.IOException;

/**
 * 1_Q4_401_Wetterdaten
 * 
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public enum LogType {
    LOG_DEBUG("Debug"),
    LOG_ERROR("Error"),
    LOG_WARNING("Warning"),
    LOG_INFO("Info"),
    LOG_UNIMPORTANT("Unimportant"),
    LOG_BADERROR("Badderror"),
    LOG_IOERROR("IOerror"),
    LOG_NORMAL(""),
    LOG_AUTO("");

    private final String name;
    
    private LogType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public static LogType auto(Exception ex) {
        if(ex.getClass().equals(IOException.class)) {
            return LOG_IOERROR;
        }
        else if(ex.getClass().equals(NullPointerException.class)) {
            return LOG_BADERROR;
        }
        else {
            return LOG_NORMAL;
        }
    }
}