package at.petritzdesigns.mishell.helper;

/**
 * MiShell
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class TimeInterval {

    private final Long time;

    public TimeInterval() {
        this.time = System.currentTimeMillis();
    }

    public TimeInterval(Long time) {
        this.time = time;
    }

    public Long getTime() {
        return time;
    }

    public Long calcDifference() {
        return System.currentTimeMillis() - time;
    }

    public Long calcDifference(Long diff) {
        return diff - time;
    }

    public Long calcDifference(TimeInterval ival) {
        return ival.getTime() - time;
    }
}
